#include "pgopencl.h"

PG_MODULE_MAGIC;

// Static variable declarations
static bool enable_pgopencl         = true;
static pgopencl_devices * cldevices = NULL;

// Functions
void _PG_init( void )
{
    if( !process_shared_preload_libraries_in_progress )
    {
        ereport(
            ERROR,
            (
                errcode( ERRCODE_OBJECT_NOT_IN_PREREQUISITE_STATE ),
                errmsg( "pgopencl must be loaded via shared_preload_libraries in postgresql.conf" )
            )
        );
    }

    elog(
        LOG,
        "pgopencl version %s loaded",
        PGOPENCL_VERSION
    );

    pgopencl_init_devices( &cldevices );

    if( cldevices == NULL || cldevices->device_count == 0 )
    {
        // Devices failed to initialize
        elog(
            WARNING,
            "No OpenCL devices found on system"
        );
        enable_pgopencl = false;
        return;
    }

    DefineCustomBoolVariable(
        "enable_pgopencl",
        "",
        NULL,
        &enable_pgopencl,
        true,
        PGC_USERSET,
        GUC_NOT_IN_SAMPLE,
        NULL,
        NULL,
        NULL
    );

    // TODO: Add hooks for CustomExecMethods,
    // Joins, etc
    return;
}

#ifndef PGOPENCL_H
#define PGOPENCL_H

#include "postgres.h"
#include "miscadmin.h"
#include "utils/guc.h"

/*
#include "access/relscan.h"
#include "access/sysattr.h"
#include "access/xact.h"
#include "access/hash.h"

#include "catalog/heap.h"
#include "catalog/pg_namespace.h"
#include "catalog/pg_type.h"

#include "fmgr.h"
#include "parser/parsetree.h"
#include "executor/nodeCustom.h"

#include "nodes/makefuncs.h"
#include "nodes/nodeFuncs.h"

#include "optimizer/clauses.h"
#include "optimizer/paths.h"
#include "optimizer/plancat.h"
#include "optimizer/restrictinfo.h"
#include "optimizer/var.h"
#include "optimizer/clauses.h"
#include "optimizer/cost.h"
#include "optimizer/pathnode.h"
#include "optimizer/planner.h"

#include "utils/builtins.h"
#include "utils/memutils.h"
#include "utils/rel.h"
#include "utils/ruleutils.h"
#include "utils/spccache.h"
#include "utils/lsyscache.h"

#include "storage/bufmgr.h"
#include "storage/ipc.h"
#include "storage/pg_shmem.h"
*/

#include "opencl.h"

#define PGOPENCL_VERSION "0.1"

// Callback Setup
void _PG_init( void );
#endif // PGOPENCL_H

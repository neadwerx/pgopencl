/*
 * For reference
 *     datatypes / typedefs / structs : see header files
 *     for general optimizer workflow, see:
 *         backend/optimizer/README
 * 
 * Path: 
 */
#include "opencl_join.h"

// Define JOIN hook
static set_join_pathlist_hook_type set_join_pathlist_next;

static CustomPathMethods opencl_join_cpm;
static CustomScanMethods opencl_join_csm;
static CustomExecMethods opencl_join_cem;

// GUCs
static bool enable_opencl_join_nested_loop;
static bool enable_opencl_join_hash;
static bool enable_opencl_join;


// Hook entry
static void opencl_join_add_join_path(
    PlannerInfo *       plan_root,
    RelOptInfo *        join_relation,
    RelOptInfo *        outer_relation,
    RelOptInfo *        inner_relation,
    JoinType            join_type,
    JoinPathExtraData * extra_data
)
{
    Path *     outer_path = NULL;
    Path *     inner_path = NULL;
    ListCell * list_cell1 = NULL;
    ListCell * list_cell2 = NULL;

    if( set_join_pathlist_next )
    {
        // Another module has invoked the CSM/CPM API
        set_join_pathlist_next(
            plan_root,
            join_relation,
            outer_relation,
            inner_relation,
            join_type,
            extra_data
        );
    }

    if(
            !enable_pgopencl
         || !enable_opencl_join
         || (
                !enable_opencl_join_hash
             && !enable_opencl_join_nested_loop
            )
      )
    {
        return;
    }

    return;
}

// Initialize the CustomScan * object, pass out a plan node, invoked on every (optimizable) query
// Plans are converted from Path nodes
Plan * opencl_join_path_to_plan(
    PlannerInfo * plan_root,
    RelOptInfo *  relation,
    CustomPath *  best_path,
    List *        target_list,
    List *        clauses,
    List *        custom_plans
)
{
    CustomScan *              custom_scan = NULL;
    struct opencl_join_path * cljoin_path = NULL; // Child of CustomPath struct
 
    cljoin_path = ( struct opencl_join_path * ) best_path;

    custom_scan                  = makeNode( CustomScan );
    custom_scan->plan.targetlist = target_list;
    custom_scan->plan.qual       = NIL;
    custom_scan->flags           = best_path->flags;
    custom_scan->methods         = &opencl_join_csm; // Add hooks for generating CustomScanState
    custom_scan->custom_plans    = list_copy_tail( custom_plans, 1 );

    custom_scan->scanrelid       = relation->relid;
    custom_scan->scan.plan.qual  = extract_actual_clauses( clauses, false );

    custom_scan->custom_private  = best_path->custom_private;

    // TODO: Can optimize I/O by performing scan of simple outer relation here
    return &custom_scan->scan.plan;
}

#if PG_VERSION_NUM >= 1100000
List * opencl_reparameterize_custom_path_by_child(
    PlannerInfo * root,
    List *        custom_private,
    RelOptInfo *  child_rel
)
{
    return NULL;
}
#endif

Node * opencl_join_generate_custom_scan_state( CustomScan * custom_scan )
{
    struct opencl_join_state * cljoin_state = NULL;

    cljoin_state = ( struct opencl_join_state * ) palloc0(
        sizeof( struct opencl_join_state )
    );
    
    if( cljoin_state == NULL )
    {
        erreport(
            ERROR,
            (
                errcode( ERRCODE_OUT_OF_MEMORY ),
                errmsg( "Could not allocate memory for custom scan state" )
            )
        );
    }
    
    NodeSetTag( cljoin_state, T_CustomScanState );

    cljoin_state->css.flags   = custom_scan->flags;
    cljoin_state->css.methods = &opencl_join_cem;

    return ( Node * ) &( cljoin->css );
}

void opencl_join_begin_custom_scan(
    CustomScanState * custom_scan_state,
    EState *          e_state,
    int               e_flags
)
{
    struct opencl_join_state * cljoin_state = NULL;
    CustomScan *               custom_scan  = NULL;

    cljoin_state = ( struct opencl_join_state * ) custom_scan_state;
    custom_scan  = ( CustomScan * ) custom_scan_state->ss.ps.plan;

    // For now, since we are not doing any custom relation scans, we will let
    // the core implementation handle the readin of data.
    
    cljoin_state->cljoin_quals = ( List * ) ExecInitExpr(
        ( Expr * ) custom_scan->custom_exprs,
        &custom_scan_state->ss.ps
    );

    return;
}

TupleTableSlot * opencl_join_exec_custom_scan(
    CustomScanState * custom_scan_state
)
{
    struct opencl_join_state * cljoin_state = NULL;
    TableTupleSlot * tuple_slot = NULL;

    cljoin_state = ( struct opencl_join_state * ) custom_scan_state;

    // We'll need to setup the OpenCL devices context and begin execution here.

    tuple_slot = ExecScan(
        &( custom_scan_state->ss ),
        ( ExecScanAccessMtd ) opencl_join_access_custom_scan,
        ( ExecScanRecheckMtd ) opencl_join_recheck_join
    );

    return tuple_slot;
}

void opencl_join_end_custom_scan( CustomScanState * custom_scan_state )
{
    return;
}

void opencl_join_rescan_custom_scan( CustomScanState * custom_scan_state )
{
    return;
}

void opencl_join_mark_position( CustomScanState * custom_scan_state )
{
    return;
}

void opencl_join_restore_position( CustomScanState * custom_scan_state )
{
    return;
}

Size opencl_join_estimate_dsm(
    CustomScanState * custom_scan_state,
    ParallelContext * context
)
{
    return 0;
}

void opencl_join_initialize_dsm(
    CustomScanState * custom_scan_state,
    ParallelContext * context,
    void *            coordinate
)
{
    return;
}

#if PG_VERSION_NUM >= 100000
void opencl_join_reinitialize_dsm(
    CustomScanState * custom_scan_state,
    ParallelContext * context,
    void *            coordinate
)
{
    return;
}

void opencl_join_shutdown( CustomScanState * custom_scan_state )
{
    return;
}
#endif

void opencl_join_initialize_worker(
    CustomScanState * custom_scan_state,
    shm_toc *         shm,
    void *            coordinate
)
{
    return;
}

void opencl_join_explain(
    CustomScanState * custom_scan_state,
    List *            ancestors,
    ExplainState *    explain
)
{
    return;
}

void opencl_join_init( void )
{
    // Setup CustomPathMethods
    opencl_join_cpm.CustomName                      = "OpenCL Join";
    opencl_join_cpm.PlanCustomPath                  = opencl_join_path_to_plan;
#if PG_VERSION_NUM >= 110000
    opencl_join_cpm.ReparameterizeCustomPathByChild = opencl_reparameterize_custom_path_by_child;
#endif
#if PG_VERSION_NUM < 90600
    opencl_join_cpm.TextOutCustomPath               = NULL;
#endif

    // Setup CustomScanMethods
    opencl_join_csm.CustomName            = "OpenCL Join";
    opencl_join_csm.CreateCustomScanState = opencl_join_generate_custom_scan_state;
#if PG_VERSION_NUM < 90600
    opencl_join_csm.TextOutCustomPath     = NULL;
#endif

    // Setup CustomExecMethods
    opencl_join_cem.CustomName                 = "OpenCL Join";
    opencl_join_cem.BeginCustomScan            = opencl_join_begin_custom_scan;
    opencl_join_cem.ExecCustomScan             = opencl_join_exec_custom_scan;
    opencl_join_cem.EndCustomScan              = opencl_join_end_custom_scan;
    opencl_join_cem.ReScanCustomScan           = opencl_join_rescan_custom_scan;
    opencl_join_cem.MarkPosCustomScan          = opencl_join_mark_position;
    opencl_join_cem.RestrPosCustomScan         = opencl_join_restore_position;
    opencl_join_cem.EstimateDSMCustomScan      = opencl_join_estimate_dsm;
    opencl_join_cem.InitializeDSMCustomScan    = opencl_join_initialize_dsm;
#if PG_VERSION_NUM >= 100000
    opencl_join_cem.ReInitializeDSMCustomScan  = opencl_join_reinitialize_dsm;
    opencl_join_cem.ShutdownCustomScan         = opencl_join_shutdown;
#endif
    opencl_join_cem.InitializeWorkerCustomScan = opencl_join_initialize_worker;
    opencl_join_cem.ExplainCustomScan          = opencl_join_explain;

    // Register
    RegisterCustomScanMethods( &opencl_join_csm );
    set_join_pathlist_next = set_join_pathlist_hook;
    set_join_pathlist_hook = opencl_join_add_join_path;

    return;
}

static TupleTableSlot * opencl_join_access_custom_scan( CustomScanState * custom_scan_state )
{
    struct opencl_join_state * cljoin_state = NULL;
    HeapScanDesc               scan         = {0};
    HeapTuple                  tuple        = {0};
    EState *                   exec_state   = NULL;
    ScanDirection              direction    = {0};
    TupleTableSlot *           tuple_slot   = NULL;

    cljoin_state = ( struct opencl_join_state * ) custom_scan_state;
    exec_state   = custom_scan_state->ss.ps.state;
    direction    = exec_state->es_direction;

    if( !custom_scan_state->css.ss.ss_currentScanDesc )
    {
        opencl_join_rescan_custom_scan( custom_scan_state );
    }

    scan = ( HeapScanDesc ) custom_scan_state->css.ss.ss_currentScanDesc;

    Assert( scan != NULL );

    // Get next tuple
    tuple = heap_getnext( scan, direction );
    ExecStoreTuple( tuple, slot, scan->rs_cbuf, false );

    return slot;
}

static bool opencl_join_recheck_join( CustomScanState * custom_scan_state, TupleTableSlot * slot )
{
    struct opencl_join_state * cljoin_state = NULL;

    cljoin_state = ( struct opencl_join_state * ) custom_scan_state;

    return true;
}

static inline void serialize_data( CustomScan * custom_scan, struct opencl_join_data * cljoin_data )
{
    List * private_data    = NIL;
    List * expression_tree = NIL;
    
    private_data = lappend( private_data, cljoin_data->device );

    custom_scan->custom_private = private_data;
    custom_scan->custom_exprs   = expression_tree;

    // TODO: pfree( cljoin_data )???
    return;
}

static inline struct opencl_join_data * deserialize_data( CustomScan * custom_scan )
{
    struct opencl_join_data * cljoin_data     = NULL;
    List *                    private_data    = NULL;
    List *                    expression_tree = NULL;
    int                       attribute_index = 0;

    cljoin_data = ( struct opencl_join_data * ) palloc0(
        sizeof( struct opencl_join_data )
    );

    if( cljoin_data == NULL )
    {
        ereport(
            ERROR,
            (
                errcode( ERRCODE_OUT_OF_MEMORY ),
                errmsg( "Failed to allocate memory OpenCL state deserialize operation" )
            )
        );
    }

    private_data    = custom_scan->custom_private;
    expression_tree = custom_scan->custom_exprs;

    // TODO: May need to hijack strVal() to store pointer to memory
    cljoin_data->device = ( pgopencl_device * ) intVal(
        list_nth( private_data, attribute_index++ )
    );

    return cljoin_data;
}

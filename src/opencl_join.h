#ifndef OPENCL_JOIN_H
#define OPENCL_JOIN_H
#include "opencl.h"                 // OpenCL Types
#include "nodes/extensible.h"       // CustomPathMethods / CustomExecMathods / CustomScanMethods API
// #include "nodes/value.h" // Possibly needed for list funcs
// #include "nodes/relation.h"         // Path
#include "funcapi.h"                // TupleTableSlot
#include "executor/nodeCustom.h"    // CustomScanState
#include "optimizer/paths.h"        // set_join_pathlist_hook_type

// For extension of the child class CustomPath, which is an extension of the Path class
//
// This inheritance uses a C hack where the first struct attribute is the instantiated
// parent, and following attributes are the child's.
struct opencl_join_path {
    CustomPath custom_path;
};

// OpenCL state information that lives for the duration of a query. This
// information is initialized, then stored in CustomScan::custom_private as a
// List *, with each attribute stored as a list element
struct opencl_join_data {
    pgopencl_device * device;
};

struct opencl_join_state {
    CustomScanState css;
    List *          cljoin_quals;
};

// conversion routines for opencl_join_data <-> CustomScan->custom_private
static inline void serialize_data( CustomScan *, struct opencl_join_data * );
static inline struct opencl_join_data * deserialize_data( CustomScan * );

static void opencl_join_add_join_path( PlannerInfo *, RelOptInfo *, RelOptInfo *, RelOptInfo *, JoinType, JoinPathExtraData * );

// opencl_join_cpm chain ( CustomPathMethods )
Plan * opencl_join_path_to_plan( PlannerInfo *, RelOptInfo *, CustomPath *, List *, List *, List * ); // Registered to PlanCustomPath
#if PG_VERSION_NUM >= 1100000
List * opencl_reparameterize_custom_path_by_child( PlannerInfo *, List *, RelOptInfo * );
#endif

// opencl_join_csm chain ( CustomScanMethods )
Node * opencl_join_generate_custom_scan_state( CustomScan * ); // Registered to CreateCustomScanState

// opencl_join_cem chain ( CustomExecMethods )
void opencl_join_begin_custom_scan( CustomScanState *, EState *, int ); // Registered to BeginCustomScan
TupleTableSlot * opencl_join_exec_custom_scan( CustomScanState * ); // Registered to ExecCustomScan
void opencl_join_end_custom_scan( CustomScanState * ); // Registered to EndCustomScan
void opencl_join_rescan_custom_scan( CustomScanState * ); // Registered to ReScanCustomScan
void opencl_join_mark_position( CustomScanState * ); // Registered to MarkPosCustomScan
void opencl_join_restore_position( CustomScanState * ); // Registered to RestrPosCustomScan
Size opencl_join_estimate_dsm( CustomScanState *, ParallelContext * ); // Registered to EstimateDSMCustomScan
void opencl_join_initialize_dsm( CustomScanState *, ParallelContext *, void * ); // Registered to InitializeDSMCustomScan
#if PG_VERSION_NUM >= 100000
void opencl_join_reinitialize_dsm( CustomScanState *, ParallelContext *, void * ); // Registered to ReInitializeDSMCustomScan
void opencl_join_shutdown( CustomScanState * ); // Registered to ShutdownCustomScan
#endif
void opencl_join_initialize_worker( CustomScanState *, shm_toc *, void * ); // Registered to InitializeWorkerCustomScan
void opencl_join_explain( CustomScanState *, List *, ExplainState * ); // Registered to ExplainCustomScan

// Entrypoint
void opencl_join_init( void );

static TupleTableSlot * opencl_join_access_custom_scan( CustomScanState * ); // Implements Access Method for OpenCL Join
static bool opencl_join_recheck_join( CustomScanState *, TupleTableSlot * ); // Implements recheck for OpenCL Join
#endif // OPENCL_JOIN_H

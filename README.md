# pgopencl

Add OpenCL support to PostgreSQL:

* JOIN paths
* Scan methods
* Operand evaluations
* Proceedural language

This project seeks to:

* Be maximally inclusive of all hardware from all vendors
* Attempt to use all hardware, regardless of supported OpenCL version
* Support hetergeneous hardware configurations
* Implement PL/pgOpenCL

# Installation

You will need the OpenCL headers and drivers for all devices you intend to use:

## CentOS / RHEL:

* opencl-headers

## Ubuntu

* todo

# Tested hardware:

## Intel

* Sandy Bridge (E5-2450L, E5-2670)
* Westmere (X5687)

## Nvidia

* Maxwell (GTX 970)

## AMD

* Polaris 20 XL (RX 580)

PG_CONFIG		?= pg_config
PG_VERSION_NUM	= $(shell $(PG_CONFIG) --version \
                  | awk '{print $$NF}' \
                  | set -e 's/\./ /g' -e 's/[A-Za-z].*$$//g' \
				  | awk '{printf "%d%02d%02d", $$1, $$2, (NF >= 3 )? $$3 : 0}' )
PGLIBDIR     	= $(shell $(PG_CONFIG) --libdir)
PGINCLUDEDIR 	= $(shell $(PG_CONFIG) --includedir)

# TODO: Add dynamic lookup based on Distro, HW vendor
CLLIBDIR     = /usr/include/CL/
CLINCLUDEDIR = /usr/lib/x86_64-linux-gnu/

CC           = gcc
LIBS         = -lm -lOpenCL
CFLAGS       = -I./src/ -I./src/lib/ -I$(PGINCLUDEDIR) -g -I$(CLINCLUDEDIR)
MODULE_big   = pgopencl
SRCS		 = src/pgopencl.c src/opencl.c src/opencl_join.c
OBJS		 = $(SRCS:.c=.o)

PG_CFLAGS	= -I$(PGINCLUDEDIR) $(CFLAGS)
PG_CPPFLAGS = -DDEBUG -g $(PG_CLAGS) $(LIBS)
EXTENSION   = pgopencl
EXTVERSION  = 0.1
EXTRA_CLEAN = src/lib/*.o src/*.o *.o

PGXS := $(shell $(PG_CONFIG) --pgxs)

include $(PGXS)
